package com.m3.training.constructors;

public class Animal  {

	public Animal() {
		super();
	}

	public String eat() {
		return "I am an animal and I eat.";
	}
}
