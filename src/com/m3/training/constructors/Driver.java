package com.m3.training.constructors;

public class Driver {

	public static void main(String[] args) {
		
		String myString = new String();
		System.out.println("empty string [" + myString + "]");
		Object obj = new Object();
		Animal animal = new Animal();
		Dog dog = new Dog();
		
		System.out.println(obj.toString());
		System.out.println(animal.toString());
		System.out.println(dog.toString());
		
		Animal animalo = new Dog();
		
		obj = "Hello";
		obj = animalo;
		obj = dog;
		
		animalo = dog;
		//.....
		animalo = new Tiger();
		// .........
		if (animalo instanceof Dog) {
			System.out.println(((Dog)animalo).bark());
		}
		
		ShibaInu doge = new ShibaInu();
		System.out.println(doge.bark());
		System.out.println("driver class in same package can use shibainu ref to get protected number:");
		System.out.println(doge.getProtectedNumber());
		System.out.println("done with protected number");
		System.out.println(dog.fetch());
		ToyPoodle prince1 = new ToyPoodle();
		prince1.fetch();
		System.out.println();
		
	}

}
