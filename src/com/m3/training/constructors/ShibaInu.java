package com.m3.training.constructors;

public class ShibaInu extends Dog {

	public ShibaInu()  {
		System.out.println("My private number is " + getProtectedNumber());
	}

	@Override
	public String bark() {
		return "much wow";
	}
	
	@Override
	public String fetch() {
		return "much run very fetch such happy";
	}
}
