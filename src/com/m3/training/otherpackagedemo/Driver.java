package com.m3.training.otherpackagedemo;

import com.m3.training.constructors.Dog;
import com.m3.training.constructors.ShibaInu;
import com.m3.training.constructors.ToyPoodle;

public class Driver {

	public Driver() {

	}
	
	public static void main(String[] args) {
		ShibaInu doge = new ShibaInu();
		System.out.println(doge.bark());
		System.out.println("driver class in different package can use shibainu ref to get protected number:");
		//System.out.println(doge.getProtectedNumber());
		System.out.println("done with protected number");
		
		GermanShepherd owen = new GermanShepherd();
		GermanShepherd chandler = new GermanShepherd();		
		//owen.getProtectedNumber();
		

		System.out.println(chandler.eat());
		
		Dog dog = owen;
		System.out.println(dog.eat());
		System.out.println(doge.eat());
		System.out.println(doge.fetch());	
		ToyPoodle prince2 = new ToyPoodle();
		//prince2.fetch();
	}

}
