package com.m3.training.otherpackagedemo;

import com.m3.training.constructors.Dog;

public class GermanShepherd extends Dog {

	public GermanShepherd() {
		System.out.println("Hello I am a German Shepherd. My private number is " + super.getProtectedNumber());
	}

	@Override
	public String eat() {
		String msg = super.eat();
		return msg + " I am a German Shepherd!";
	}
}
